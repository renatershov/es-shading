import common.ESDriver;
import esv2.ESDriverV2;
import esv6.ESDriverV6;

import java.net.UnknownHostException;

public class ESDriverFactory {

    public ESDriver get(ESVersion version) throws UnknownHostException {
        if (version == ESVersion.ESv2)
            return new ESDriverV2();
        else if (version == ESVersion.ESv6)
            return new ESDriverV6();
        else {
            return null;
        }
    }
}
