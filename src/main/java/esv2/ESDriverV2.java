package esv2;

import common.ESDriver;
import org.es23.elasticsearch.Version;
import org.es23.elasticsearch.action.admin.cluster.node.info.NodesInfoResponse;
import org.es23.elasticsearch.client.transport.TransportClient;
import org.es23.elasticsearch.common.settings.Settings;
import org.es23.elasticsearch.common.transport.InetSocketTransportAddress;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ESDriverV2 implements ESDriver {
    private TransportClient client;

    public ESDriverV2() throws UnknownHostException {
        Settings settings = Settings.builder()
                .put("cluster.name", "reltiocluster").build();
        this.client = TransportClient.builder().settings(settings).build();
        client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
    }

    public String clusterVersion() throws UnknownHostException {
        NodesInfoResponse nodesInfoResponse = client.admin().cluster().prepareNodesInfo().execute().actionGet();
        Version version = nodesInfoResponse.getNodes()[0].getVersion();
        return version.toString();
    }

    public void close() {
        client.close();
    }
}
