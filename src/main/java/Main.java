import common.ESDriver;

import java.net.UnknownHostException;

//import org.elasticsearch.action.admin.cluster.stats.ClusterStatsResponse;
//import org.elasticsearch.client.transport.TransportClient;
//import org.elasticsearch.cluster.node.DiscoveryNode;
//import org.elasticsearch.common.settings.Settings;
//import org.elasticsearch.common.transport.InetSocketTransportAddress;
//import org.elasticsearch.transport.client.PreBuiltTransportClient;

public class Main {
    public static void main(String[] args) throws UnknownHostException {

        ESDriver esDriverV2 = new ESDriverFactory().get(ESVersion.ESv2);
        ESDriver esDriverV6 = new ESDriverFactory().get(ESVersion.ESv6);

        {
            String cstats = esDriverV2.clusterVersion();
            System.out.println("ESV2 " + cstats);
            esDriverV2.close();
        }

        {
            String cstats = esDriverV6.clusterVersion();
            System.out.println("ESV6 " + cstats);
            esDriverV6.close();
        }


    }
}
