package esv6;


import common.ESDriver;

import org.es6.elasticsearch.Version;
import org.es6.elasticsearch.action.admin.cluster.node.info.NodesInfoResponse;
import org.es6.elasticsearch.client.transport.TransportClient;
import org.es6.elasticsearch.common.settings.Settings;

import org.es6.elasticsearch.common.transport.TransportAddress;
import org.es6.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ESDriverV6 implements ESDriver {
    private TransportClient client;

    public ESDriverV6() throws UnknownHostException {
        Settings settings = Settings.builder()
                .put("cluster.name", "docker-cluster")
                .build();
        this.client = new PreBuiltTransportClient(settings);
        client.addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9500));
    }

    public String clusterVersion() throws UnknownHostException {
        NodesInfoResponse nodesInfoResponse = client.admin().cluster().prepareNodesInfo().execute().actionGet();
        Version version = nodesInfoResponse.getNodes().iterator().next().getVersion();
        return version.toString();
    }

    public void close() {
        client.close();
    }
}
