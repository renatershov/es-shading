package common;

import java.net.UnknownHostException;

public interface ESDriver {

    public String clusterVersion() throws UnknownHostException;

    public void close();
}
